To split a pane vertically in tmux, you can use the following key combination:

Ctrl + b, %

This will split the current pane into two vertical panes. You can then navigate between the panes using the following key combinations:

Ctrl + b, Left Arrow - Move to the left pane
Ctrl + b, Right Arrow - Move to the right pane

You can also resize the panes by holding down Ctrl + b and pressing one of the arrow keys to move the divider between them.
# Tue  2 Jul 17:18:59 CEST 2024 - tmux split pane vertically